function preload() {
  img = loadImage('art.jpg')
}

function setup() {
  // put setup code here
  createCanvas(500,500);
  // Creates a canvas with the size (500 x 500)
  background(600,100,100);
  // Creates a backgroundcolor where:
    // red = 600, green = 100 and blue = 100

  let button = createButton('Click me');
  // Creates a button and gives the value "CreateButton" to it. On top of it there is the text "Click me".
  button.position(215,245)
  // Creates the position for the button.
  button.mousePressed(() => {
    // Uses the function "button.mousePressed"
    let bg = random(['pink','lightblue','lightgreen','lightyellow']);
    // Assigns the values 'pink', 'lightblue', 'lightgreen' and 'lightyellow' to the variable "bg"
    background (bg);
    // Defines background as "bg".
  }); 
}

 function draw() {
   // put drawing code here
   const circleA = color(250, 250, 250);
   fill(circleA);
   // fills the circle wih the previous defined color.
   noStroke();
   // Creates no outline for the object.
   circle (80,80,120,120);
    // Creates a circle with the position (80, 80) and the size (120, 120)

    // repitition of the previous commands.
   const circleB = color(250, 250, 250);
   fill(circleB);
   noStroke();
   circle(240,240,200,200);

   const circleC = color(250, 250, 250);
   fill(circleC);
   noStroke();
   circle(400,400,120,120);

   const rectangleA = color(0, 0, 0);
   fill(rectangleA);
   noStroke();
   rect(350, 30, 100, 100, 20);
   // Creates a rectangle at the coordiant (350, 30) and the size (100, 100) and roundness of 20.

   const rectangleB = color(0, 0, 0);
   fill(rectangleB);
   noStroke();
   rect(30, 350, 100, 100, 20);

   image(img, 360, 40, 80, 80);
   // This command positions the pre-defined image at (360, 40) with the size (80, 80)
   image(img, 40, 360, 80, 80);
   // Repition of previous command.
 }